"""
Name
    vac_exporter

Synopsis
    Export from a Veeam Availability Console Tenant Usage Data 
"""

import requests
import json
import datetime
import pytz
import itertools

from prometheus_client import CollectorRegistry, generate_latest
from prometheus_client.core import GaugeMetricFamily

from defer import parallelize

requests.packages.urllib3.disable_warnings()


class VacTenantCollector(object):
    """
    Class for Tenant Data Collection
    """


    def __init__(self, connection, host, ignore_ssl):
        self.host = host
        self.token = connection.token
        self.ignore_ssl = ignore_ssl
        self.tenants = connection.tenants


    def collect(self):
        """
        Collect Tenant Data
        :param self: Connection information
        :return: List of Metrics
        """
        metric_list = {}
        metric_list['tenant'] = {
                'vac_tenant_vms_backedup': GaugeMetricFamily(
                    'vac_teant_vms_backedup',
                    'Number of protected vms of tenant',
                    labels=['tenant_name','tenant_id']),
                'vac_tenant_vms_replicated': GaugeMetricFamily(
                    'vac_tenant_vms_replicated',
                    'Number of replicated vms of tenant',
                    labels=['tenant_name','tenant_id']),
                'vac_tenant_vms_backed_cloud': GaugeMetricFamily(
                    'vac_tenant_vms_backed_cloud',
                    'Number of tenat vms backed up to cloud',
                    labels=['tenat_name','tenant_id'])
                }

        metrics = {}
        for key in metric_list.keys():
            metrics.update(metric_list[key])

        """
        Get Tenant information
        """
        start = datetime.datetime.utcnow()
        log("Starting Tenat metrics collection")

        for tenant in self.tenants:
            labels = [str(tenant['id']), str(tenant['name'])]

            metrics['vac_tenant_vms_backedup'].add_metric(labels, tenant['vMsBackedUp'])
            metrics['vac_tenant_vms_replicated'].add_metric(labels, tenant['vMsReplicated'])
            metrics['vac_tenant_vms_backed_cloud'].add_metric(labels, tenant['vMsBackedUpToCloud'])

        log("Finished Tenant metrics collection (%s)", datetime.datetime.utcnow() - start)

        return list(metrics.values())


class VacJobCollector(object):
    """
    Class for Job Data Collection
    """


    def __init__(self, connection, host, ignore_ssl):
        self.host = host
        self.token = connection.token
        self.ignore_ssl = ignore_ssl


    def collect(self):
        """
        Collect Job Data
        :param self: Connection information
        :return: List of Metrics
        """
        metric_list = {}
        metric_list['job'] = {
                'vac_job_processing_rate_bytes': GaugeMetricFamily(
                    'vac_job_processing_rate_bytes',
                    'Rate of job processing in bytes',
                    labels=['job_id','job_name','tenant_id','units']),
                'vac_job_transfer_data_bytes': GaugeMetricFamily(
                    'vac_job_transfer_data_bytes',
                    'Amount of data transferred in job in bytes',
                    labels=['job_id','job_name','tenant_id','units']),
                'vac_job_protected_vms': GaugeMetricFamily(
                    'vac_job_protected_vms',
                    'Protected vms in job',
                    labels=['job_id','job_name','tenant_id'])
                }

        metrics = {}
        for key in metric_list.keys():
            metrics.update(metric_list[key])

        """
        Invoke Rest Call for Jobs
        """
        log("Gathering VAC Jobs")
        start = datetime.datetime.utcnow()
        headers = {'Authorization': self.token}
        url = "https://" + self.host + ":1281/v2/jobs"
        try:
            if self.ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Jobs (%s)",datetime.datetime.utcnow() - start)
                jobs = json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Jobs (%s)",datetime.datetime.utcnow() - start)
                jobs = json.loads(r.content)
        except Exception as error:
            log("Unable to gather jobs: " + str(error))


        """
        Get Job information
        """
        log("Starting Job metrics collection")
        start = datetime.datetime.utcnow()

        labels = {}
        try:
            for job in jobs:
                tenant_id = job['_links']['tenants']['href'].split("/")

                processing_rate = str(job['processingRate']) + " " + job['processingRateUnits']
                transferred_rate = str(job['transferredData']) + " " + job['transferredDataUnits']
                defs = {'GB/s':1024**3, 'MB/s':1024**2, 'KB/s':1024, 'B/s':1, 'GB':1024**3, 'MB':1024**2, 'KB':1024, 'B':1}

                processing_rate_bytes = processing_rate.split()
                processing_rate_bytes = float(processing_rate_bytes[0]) * defs[processing_rate_bytes[1]]
                transferred_rate_bytes = transferred_rate.split()
                transferred_rate_bytes = float(transferred_rate_bytes[0]) * defs[transferred_rate_bytes[1]]

                #labels_processing = [str(job['id']),str(job['name']),str(tenant_id[(len(tenant_id)-1)]),str(job['processingRateUnits'])]
                #labels_transferred = [str(job['id']),str(job['name']),str(tenant_id[(len(tenant_id)-1)]),str(job['transferredDataUnits'])]
                labels = [str(job['id']),str(job['name']),str(tenant_id[(len(tenant_id)-1)])]

                metrics['vac_job_processing_rate_bytes'].add_metric(labels, float(processing_rate_bytes))
                metrics['vac_job_transfer_data_bytes'].add_metric(labels, float(transferred_rate_bytes))
                metrics['vac_job_protected_vms'].add_metric(labels, job['protectedVMs'])
        except Exception as error:
            log("Unable to parse job: " + str(error))

        log("Finished Job metrics collection (%s)", datetime.datetime.utcnow() - start)

        return list(metrics.values())


class VacResourceCollector(object):
    """
    Class for Backup Resource Data Collection
    """


    def __init__(self, connection, host, ignore_ssl):
        self.host = host
        self.token = connection.token
        self.ignore_ssl = ignore_ssl
        self.tenants = connection.tenants


    def collect(self):
        """
        Collect Backup Resources Data
        :param self: Connection information
        :return: List of Metrics
        """
        metric_list = {}
        metric_list['resources'] = {
                'vac_resources_storage_quota_bytes': GaugeMetricFamily(
                    'vac_resources_storage_quota_bytes',
                    'Storage quota in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_vms_quota': GaugeMetricFamily(
                    'vac_resources_vms_quota',
                    'VM quota for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_traffic_quota_bytes': GaugeMetricFamily(
                    'vac_resources_traffic_quota_bytes',
                    'Network traffic quota in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_used_storage_bytes': GaugeMetricFamily(
                    'vac_resources_used_storage_bytes',
                    'Used storage in bytes for tenant resources',
                    labels=['resource_id', 'tenant_id','tenant_name','units']),
                'vac_resources_used_traffic_bytes': GaugeMetricFamily(
                    'vac_resources_used_traffic_bytes',
                    'Used network traffic in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units'])
                }
        metrics = {}
        for key in metric_list.keys():
            metrics.update(metric_list[key])

        """
        Get Tenant Resource information
        """
        log("Starting Resource collection")
        start = datetime.datetime.utcnow()

        labels = {}
        for tenant in self.tenants:
            try:
                resources = self.tenant_resources(tenant['id'])
                labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name'])]

                used_storage_quota_rate = str(resources[0]['usedStorageQuota']) + " " + resources[0]['usedStorageQuotaUnits']
                used_traffic_quota_rate = str(resources[0]['usedTrafficQuota']) + " " + resources[0]['usedTrafficQuotaUnits']
                traffic_quota_rate = str(resources[0]['trafficQuota']) + " " + resources[0]['trafficQuotaUnits']
                storage_quota_rate = str(resources[0]['storageQuota']) + " " + resources[0]['storageQuotaUnits']

                defs = {'GB/s':1024**3, 'MB/s':1024**2, 'KB/s':1024, 'B/s':1, 'GB':1024**3, 'MB':1024**2, 'KB':1024, 'B':1}
                
                used_storage_quota_bytes = used_storage_quota_rate.split()
                used_storage_quota_bytes = float(used_storage_quota_bytes[0]) * defs[used_storage_quota_bytes[1]] 
                used_traffic_quota_bytes = used_traffic_quota_rate.split()
                used_traffic_quota_bytes = float(used_traffic_quota_bytes[0]) * defs[used_traffic_quota_bytes[1]]
                traffic_quota_bytes = traffic_quota_rate.split()
                traffic_quota_bytes = float(traffic_quota_bytes[0]) * defs[traffic_quota_bytes[1]]
                storage_quota_bytes = storage_quota_rate.split()
                storage_quota_bytes = float(storage_quota_bytes[0]) * defs[storage_quota_bytes[1]]
                #storage_quota_labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name']),str(resources[0]['storageQuotaUnits'])]
                #storage_labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name']),str(resources[0]['usedStorageQuotaUnits'])]
                #traffic_quota_labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name']),str(resources[0]['trafficQuotaUnits'])]
                #traffic_labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name']),str(resources[0]['usedTrafficQuotaUnits'])]

                metrics['vac_resources_storage_quota_bytes'].add_metric(labels, float(storage_quota_bytes))
                metrics['vac_resources_vms_quota'].add_metric(labels, resources[0]['vMsQuota'])
                metrics['vac_resources_traffic_quota_bytes'].add_metric(labels, float(traffic_quota_bytes))
                metrics['vac_resources_used_storage_bytes'].add_metric(labels, float(used_storage_quota_bytes))
                metrics['vac_resources_used_traffic_bytes'].add_metric(labels, float(used_traffic_quota_bytes))
            except Exception as error:
                log("Unable to gather resources for (%s) (%s): (%s)", tenant['id'], tenant['name'], error)
                continue

        return list(metrics.values()) 


    def tenant_resources(self, tenant_id):
        """
        Invoke Rest call for Tenant Resources
        """
        log("Gathering resources for tenant {}".format(tenant_id))
        start = datetime.datetime.utcnow()
        headers = {'Authorization': self.token}
        url = "https://" + self.host + ":1281/v2/tenants/" + str(tenant_id) + "/backupResources"
        try:
            if self.ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Tenant resources (%s)", datetime.datetime.utcnow() - start)
                return json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Tenant resources (%s)", datetime.datetime.utcnow() - start)
                return json.loads(r.content)
        except Exception as error:
            log("Unable to gather tenant resoucres: " + str(error))


class VacConnection(object):
    """
    Class For VAC API connection, and most used data
    """


    def __init__(self, host, username, password, ignore_ssl):
        self.host = host
        self.username = username
        self.password = password
        self.ignore_ssl = ignore_ssl
        self.token = ''
        self.tenants = {}
        self.configure()


    def configure(self):
        """
        Connect to VAC and get connection
        """
        body = "grant_type=password&username=" + self.username + "&password=" + self.password
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer'}
        url = "https://" + self.host + ":1281/token"
        try:
            if self.ignore_ssl:
                # Request api key with sslverify = false
                r = requests.post(url, data=body, headers=headers, verify=False)
                response = json.loads(r.content)
                self.token = "Bearer " + response['access_token']
            else:
                r = requests.post(url, data=body, headers=headers)
                response = json.loads(r.content)
                self.token = "Bearer " + response['access_token']
        except Exception as error:
            log("Unable to gather token: " + str(error))

        """
        Invoke Rest call for Tenants
        """
        log("Gathering VAC Tenants")
        start = datetime.datetime.utcnow()
        headers = {'Authorization': self.token}
        url = "https://" + self.host + ":1281/v2/tenants"
        try:
            if self.ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Tenants (%s)", datetime.datetime.utcnow() - start)
                self.tenants = json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Tenants (%s)", datetime.datetime.utcnow() - start)
                self.tenants = json.loads(r.content)
        except Exception as error:
            log("Unable to gather tenants: " + str(error))


def log(data, *args):
    """
    Log any message in a uniform format
    """
    print("[{0}] {1}".format(datetime.datetime.utcnow().replace(tzinfo=pytz.utc), data % args))


def collect_vac(host, username, password, ignore_ssl):
    """
    Scrape and return metrics 
    """
    connection = VacConnection(host, username, password, ignore_ssl)
    
    registry = CollectorRegistry()
    registry.register(VacTenantCollector(connection, host, ignore_ssl))
    registry.register(VacJobCollector(connection, host, ignore_ssl))
    registry.register(VacResourceCollector(connection, host, ignore_ssl))
    return generate_latest(registry)
