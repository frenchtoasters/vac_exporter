FROM python:3.6-alpine

LABEL MAINTAINER="Tyler French <tylerfrench2@gmail.com>"
LABEL NAME=vac_exporter

WORKDIR /opt/vac_exporter/
COPY . /opt/vac_exporter/

RUN set -x; buildDeps="gcc python-dev musl-dev libffi-dev openssl openssl-dev" \
 && apk add --no-cache --update $buildDeps \
 && pip install -r requirements.txt . \
 && apk del $buildDeps

EXPOSE 9273

ENV PYTHONUNBUFFERED=1

ENTRYPOINT ["/usr/local/bin/vac_exporter"]
