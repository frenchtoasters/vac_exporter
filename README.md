# vac_exporter

Veeam VAC Exporter for Prometheus. 

Get VAC Tenant information:
* Tenant Backuped VMs
* Tenant Replicated VMs
* Tenant Backed up to Cloud VMs
* Tenant Storage Quota
* Tenant VMs Quota
* Tenant Traffic Quota
* Tenant Used Storage
* Tenant Used Traffic 
* Tenant jobs Processing Rate
* Tenant jobs Transferred Data
* Tenant jobs Protected VM count

## Install

You can do the following to install this exporter:

***Note*** This is only tested working with python3.6

* Install locally

```
$> python setup.py install
$> vac_exporter -c /path/to/your/config
```

* Install via pip

```
$> python -m pip install vac-exporter
```

* Run via Docker container

```
docker run -it --rm  -p 9273:9273 -e VAC_USER=${VAC_USERNAME} -e VAC_PASSWORD=${VAC_PASSWORD} -e VAC_IGNORE_SSL=True --name vac_exporter registry.gitlab.com/frenchtoasters/vac_exporter:latest
```

## Configuration 

You should only provide a configuration file if the environment variables are not going to be set. If you are going to use a configuration file for your container you will have to edit the container ENTRYPOINT like so:

```
ENTRYPOINT ["/usr/local/bin/vac_exporter","-c","/path/to/your/config"]
```

The following is what an example configuration file would look like:

```
default:
    vac_user: "user"
    vac_password: "password"
    ignore_ssl: False
```

### Environment Variables

| Variable       | Precedence             | Defaults | Description                                       |
|----------------|------------------------|----------|---------------------------------------------------|
| VAC_USER       | config, env            | n/a      | User for connecting to vac                        |
| VAC_PASSWORD   | config, env            | n/a      | Password for connecting to vac                    |
| VAC_IGNORE_SSL | config, env            | False    | Ignore the ssl cert on the connection to vac host |


### Prometheus configuration

The following the base Prometheus configuration file.

```
  - job_name: 'veeam_vac'
    metrics_path: '/vac'
    static_configs:
      - targets:
        - 'veeam.company.com'
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: localhost:9273

# Example of Multiple VAC's usage 

- job_name: vac_export
    metrics_path: /vac
    static_configs:
    - targets:
      - vac01
      - 'vac02.example.org'
      - "vac03.com"
    relabel_configs:
    - source_labels: [__address__]
      target_label: __param_target
    - source_labels: [__param_target]
      target_label: instance
    - target_label: __address__
      replacement: exporter_ip:9273
```

# Example Output

***Thanks to a certian Lobster***

`:9273/vac`

```
$> curl 127.0.0.1:9273/vac?target=vac.duckdns.org
# HELP vac_teant_vms_backedup Number of protected vms of tenant
# TYPE vac_teant_vms_backedup gauge
vac_teant_vms_backedup{tenant_id="Tenant1",tenant_name="2"} 0.0
vac_teant_vms_backedup{tenant_id="homelab",tenant_name="4"} 45.0
# HELP vac_tenant_vms_replicated Number of replicated vms of tenant
# TYPE vac_tenant_vms_replicated gauge
vac_tenant_vms_replicated{tenant_id="Tenant1",tenant_name="2"} 0.0
vac_tenant_vms_replicated{tenant_id="homelab",tenant_name="4"} 0.0
# HELP vac_tenant_vms_backed_cloud Number of tenat vms backed up to cloud
# TYPE vac_tenant_vms_backed_cloud gauge
vac_tenant_vms_backed_cloud{tenant_id="Tenant1",tenat_name="2"} 0.0
vac_tenant_vms_backed_cloud{tenant_id="homelab",tenat_name="4"} 2.0
# HELP vac_job_processing_rate_bytes Rate of job processing in bytes
# TYPE vac_job_processing_rate_bytes gauge
vac_job_processing_rate_bytes{job_id="1",job_name="SQL_To_CloudConnect",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="2",job_name="vcd",tenant_id="4"} 312135.68
vac_job_processing_rate_bytes{job_id="3",job_name="domain-controllers",tenant_id="4"} 36071014.4
vac_job_processing_rate_bytes{job_id="4",job_name="veeam",tenant_id="4"} 35515269.12
vac_job_processing_rate_bytes{job_id="5",job_name="veeam-sp",tenant_id="4"} 36207329.28
vac_job_processing_rate_bytes{job_id="6",job_name="Testing",tenant_id="4"} 55092183.04
vac_job_processing_rate_bytes{job_id="7",job_name="mail",tenant_id="4"} 61362667.52
vac_job_processing_rate_bytes{job_id="8",job_name="vmware",tenant_id="4"} 69562531.84
vac_job_processing_rate_bytes{job_id="9",job_name="centos_no_connectivity_restore",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="10",job_name="plex",tenant_id="4"} 36029071.36
vac_job_processing_rate_bytes{job_id="11",job_name="templates",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="12",job_name="sql_aap",tenant_id="4"} 35399925.76
vac_job_processing_rate_bytes{job_id="13",job_name="ccr-seed",tenant_id="4"} 103871938.56
vac_job_processing_rate_bytes{job_id="14",job_name="test_vcd_test",tenant_id="4"} 48758784.0
vac_job_processing_rate_bytes{job_id="3",job_name="Backup_Copy_Test_3",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="4",job_name="TinyVM and CentOS SYN full",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="5",job_name="test_to_ds_clone1",tenant_id="4"} 0.0
vac_job_processing_rate_bytes{job_id="15",job_name="SureBackup Job 1",tenant_id="4"} 0.0
# HELP vac_job_transfer_data_bytes Amount of data transferred in job in bytes
# TYPE vac_job_transfer_data_bytes gauge
vac_job_transfer_data_bytes{job_id="1",job_name="SQL_To_CloudConnect",tenant_id="4"} 0.0
vac_job_transfer_data_bytes{job_id="2",job_name="vcd",tenant_id="4"} 40663777.28
vac_job_transfer_data_bytes{job_id="3",job_name="domain-controllers",tenant_id="4"} 166115409.92
vac_job_transfer_data_bytes{job_id="4",job_name="veeam",tenant_id="4"} 2383706849.28
vac_job_transfer_data_bytes{job_id="5",job_name="veeam-sp",tenant_id="4"} 737589329.92
vac_job_transfer_data_bytes{job_id="6",job_name="Testing",tenant_id="4"} 12670153523.2
vac_job_transfer_data_bytes{job_id="7",job_name="mail",tenant_id="4"} 212525383.68
vac_job_transfer_data_bytes{job_id="8",job_name="vmware",tenant_id="4"} 744184872.96
vac_job_transfer_data_bytes{job_id="9",job_name="centos_no_connectivity_restore",tenant_id="4"} 32.0
vac_job_transfer_data_bytes{job_id="10",job_name="plex",tenant_id="4"} 767610060.8
vac_job_transfer_data_bytes{job_id="11",job_name="templates",tenant_id="4"} 0.0
vac_job_transfer_data_bytes{job_id="12",job_name="sql_aap",tenant_id="4"} 11886321991.68
vac_job_transfer_data_bytes{job_id="13",job_name="ccr-seed",tenant_id="4"} 1202590842.88
vac_job_transfer_data_bytes{job_id="14",job_name="test_vcd_test",tenant_id="4"} 17480516894.72
vac_job_transfer_data_bytes{job_id="3",job_name="Backup_Copy_Test_3",tenant_id="4"} 0.0
vac_job_transfer_data_bytes{job_id="4",job_name="TinyVM and CentOS SYN full",tenant_id="4"} 64.0
vac_job_transfer_data_bytes{job_id="5",job_name="test_to_ds_clone1",tenant_id="4"} 0.0
vac_job_transfer_data_bytes{job_id="15",job_name="SureBackup Job 1",tenant_id="4"} 0.0
# HELP vac_job_protected_vms Protected vms in job
# TYPE vac_job_protected_vms gauge
vac_job_protected_vms{job_id="1",job_name="SQL_To_CloudConnect",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="2",job_name="vcd",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="3",job_name="domain-controllers",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="4",job_name="veeam",tenant_id="4"} 8.0
vac_job_protected_vms{job_id="5",job_name="veeam-sp",tenant_id="4"} 2.0
vac_job_protected_vms{job_id="6",job_name="Testing",tenant_id="4"} 17.0
vac_job_protected_vms{job_id="7",job_name="mail",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="8",job_name="vmware",tenant_id="4"} 3.0
vac_job_protected_vms{job_id="9",job_name="centos_no_connectivity_restore",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="10",job_name="plex",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="11",job_name="templates",tenant_id="4"} 6.0
vac_job_protected_vms{job_id="12",job_name="sql_aap",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="13",job_name="ccr-seed",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="14",job_name="test_vcd_test",tenant_id="4"} 3.0
vac_job_protected_vms{job_id="3",job_name="Backup_Copy_Test_3",tenant_id="4"} 1.0
vac_job_protected_vms{job_id="4",job_name="TinyVM and CentOS SYN full",tenant_id="4"} 2.0
vac_job_protected_vms{job_id="5",job_name="test_to_ds_clone1",tenant_id="4"} 0.0
vac_job_protected_vms{job_id="15",job_name="SureBackup Job 1",tenant_id="4"} 0.0
# HELP vac_resources_storage_quota_bytes Storage quota in bytes for tenant resources
# TYPE vac_resources_storage_quota_bytes gauge
vac_resources_storage_quota_bytes{resource_id="1",tenant_id="2",tenant_name="Tenant1"} 536870912000.0
# HELP resources_vms_quota VM quota for tenant resources
# TYPE resources_vms_quota gauge
vac_resources_vms_quota{resource_id="1",tenant_id="2",tenant_name="Tenant1"} 0.0
# HELP vac_resources_traffic_quota_bytes Network traffic quota in bytes for tenant resources
# TYPE vac_resources_traffic_quota_bytes gauge
vac_resources_traffic_quota_bytes{resource_id="1",tenant_id="2",tenant_name="Tenant1"} 0.0
# HELP vac_resources_used_storage_bytes Used storage in bytes for tenant resources
# TYPE vac_resources_used_storage_bytes gauge
vac_resources_used_storage_bytes{resource_id="1",tenant_id="2",tenant_name="Tenant1"} 152890098319.36
# HELP vac_resources_used_traffic_bytes Used network traffic in bytes for tenant resources
# TYPE vac_resources_used_traffic_bytes gauge
vac_resources_used_traffic_bytes{resource_id="1",tenant_id="2",tenant_name="Tenant1"} 24169676.8
```

`:9273/metrics`

```
$> curl 127.0.0.1:9273/metrics
# HELP process_virtual_memory_bytes Virtual memory size in bytes.
# TYPE process_virtual_memory_bytes gauge
process_virtual_memory_bytes 96972800.0
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 25964544.0
# HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
# TYPE process_start_time_seconds gauge
process_start_time_seconds 1549495918.93
# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
# TYPE process_cpu_seconds_total counter
process_cpu_seconds_total 0.21000000000000002
# HELP process_open_fds Number of open file descriptors.
# TYPE process_open_fds gauge
process_open_fds 6.0
# HELP process_max_fds Maximum number of open file descriptors.
# TYPE process_max_fds gauge
process_max_fds 1048576.0
# HELP vac_collection_duration_seconds Duration of collections by the VAC exporter
# TYPE vac_collection_duration_seconds summary
# HELP vac_request_errors_total Errors in requests to VAC exporter
# TYPE vac_request_errors_total counter
```

`:9273/healthz`

```
$> curl 127.0.0.1:9273/healthz
OK
```

`:9273/`

```
$> curl 127.0.0.1:9273/
VAC Exporter
Visit /vac?target=1.2.3.4 to use.
```
