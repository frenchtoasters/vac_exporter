#!/usr/bin/env python
# -*- python -*-
# -*- coding: utf-8 -*-

import datetime
import pytz
import sys
import requests
import json
import yaml
from argparse import ArgumentParser

# Twisted 
from twisted.internet import reactor, endpoints, defer, threads
from twisted.web.server import Site, NOT_DONE_YET
from twisted.web.resource import Resource

# Prometheus
from prometheus_client.core import GaugeMetricFamily
from prometheus_client import CollectorRegistry, generate_latest

# Local
from defer import parallelize

requests.packages.urllib3.disable_warnings()

def log(data, *args):
    """
    Log any message in a uniform format
    """
    print("[{0}] {1}".format(datetime.datetime.utcnow().replace(tzinfo=pytz.utc), data % args))


class ListCollector(object):
    """
    Class for returning full list of collected metrics
    """
    def __init__(self, metrics):
        self.metrics = list(metrics)


    def collect(self):
        return self.metrics


class VacApplicationResource(Resource):
    """
    Class for Vac Exporter Application configuration
    """

    def __init__(self, args):
        Resource.__init__(self)
        self.configure(args)


    def configure(self, args):
        """
        Configure connection to Vac
        :param args: arguments passed to exporter
        """
        if args.config_file:
            with open(args.config_file) as handle:
                self.config = yaml.safe_load(handle)

        else:
            """
            Set configuration from OS environement variables
            """
            self.config = {
                    'default': {
                        'vac_user': os.environ.get('VAC_USER'),
                        'vac_password': os.environ.get('VAC_PASSWORD'),
                        'ignore_ssl': os.environ.get('VAC_IGNORE_SSL', False),
                    }
                }

            for key in os.environ.keys():
                if key == 'VAC_USER':
                    continue
                if not key.startswith('VAC_') or not key.endswith('_USER'):
                    continue

                section = key.split('_', 1)[1].rsplit('_', 1)[0]

                self.config[section.lower()] = {
                    'vac_user': os.environ.get('VAC_{}_USER'.format(section)),
                    'vac_password': os.environ.get('VAC_{}_PASSWORD'.format(section)),
                    'ignore_ssl': os.environ.get('VAC_{}_IGNORE_SSL'.format(section), False),
                }

        self.section = self.config.get(b'section', [b'default'])[0].decode('utf-8')
        if self.section not in self.config.keys():
            log("{} is not a valid section, using default".format(self.section))
            self.section = 'default'
        log("Configuration complete for {0}".format(self.config[self.section].get('vac_host')))

        # Make connection to Vac endpoint configured
        vac = _set_connection(self.config[self.section].get('vac_user'), self.config[self.section].get('vac_password'), self.config[self.section].get('vac_host'), self.config[self.section].get('ignore_ssl'))
        
        def onError(err):
            log("Error connecting to Vac endpoint (%s)", err)
        vac.addErrback(onError)

        def onSuccess(connection):
            self.host = self.config[self.section].get('vac_host')
            self.ignore_ssl = self.config[self.section].get('ignore_ssl')
            self.token = connection['token']
            self.tenants = connection['tenants']
        vac.addCallback(onSuccess)
 

    def render_GET(self, request):
        """
        Render data from collector
        """
        try:
            collector = VacCollector(
                    request.args[b'target'][0].decode("utf-8"),
                    self.ignore_ssl,
                    self.token,
                    self.tenants
                )
        except:
            collector = VacCollector(
                    self.host,
                    self.ignore_ssl,
                    self.token,
                    self.tenants
                )

        metrics = collector.collect()
        def onError(err):
            log("Collection Error {}".format(err))
        metrics.addErrback(onError)

        def onSuccess(metric_list):
            registry = CollectorRegistry()
            registry.register(ListCollector(metric_list))
            output = generate_latest(registry)

            request.setHeader("Content-Type", "text/plain; charset=UTF-8")
            request.setResponseCode(200)
            request.write(output)
            request.finish()
        metrics.addCallback(onSuccess)

        return NOT_DONE_YET


class VacCollector():
    """
    Class for Vac collector
    """


    def __init__(self, host, ignore_ssl, token, tenants):
        self.host = host
        self.ignore_ssl = ignore_ssl
        self.token = token
        self.tenants = tenants


    def collect(self):
        metric_list = {}
        metric_list['tenant'] = {
                'vac_tenant_vms_backedup': GaugeMetricFamily(
                    'vac_teant_vms_backedup',
                    'Number of protected vms of tenant',
                    labels=['tenant_name','tenant_id']),
                'vac_tenant_vms_replicated': GaugeMetricFamily(
                    'vac_tenant_vms_replicated',
                    'Number of replicated vms of tenant',
                    labels=['tenant_name','tenant_id']),
                'vac_tenant_vms_backed_cloud': GaugeMetricFamily(
                    'vac_tenant_vms_backed_cloud',
                    'Number of tenat vms backed up to cloud',
                    labels=['tenant_name','tenant_id'])
                }
        metric_list['job'] = {
                'vac_job_processing_rate_bytes': GaugeMetricFamily(
                    'vac_job_processing_rate_bytes',
                    'Rate of job processing in bytes',
                    labels=['job_id','job_name','tenant_id','units']),
                'vac_job_transfer_data_bytes': GaugeMetricFamily(
                    'vac_job_transfer_data_bytes',
                    'Amount of data transferred in job in bytes',
                    labels=['job_id','job_name','tenant_id','units']),
                'vac_job_protected_vms': GaugeMetricFamily(
                    'vac_job_protected_vms',
                    'Protected vms in job',
                    labels=['job_id','job_name','tenant_id'])
                }
        metric_list['resources'] = {
                'vac_resources_storage_quota_bytes': GaugeMetricFamily(
                    'vac_resources_storage_quota_bytes',
                    'Storage quota in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_vms_quota': GaugeMetricFamily(
                    'vac_resources_vms_quota',
                    'VM quota for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_traffic_quota_bytes': GaugeMetricFamily(
                    'vac_resources_traffic_quota_bytes',
                    'Network traffic quota in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units']),
                'vac_resources_used_storage_bytes': GaugeMetricFamily(
                    'vac_resources_used_storage_bytes',
                    'Used storage in bytes for tenant resources',
                    labels=['resource_id', 'tenant_id','tenant_name','units']),
                'vac_resources_used_traffic_bytes': GaugeMetricFamily(
                    'vac_resources_used_traffic_bytes',
                    'Used network traffic in bytes for tenant resources',
                    labels=['resource_id','tenant_id','tenant_name','units'])
                }

        metrics = {}
        for key in metric_list.keys():
            metrics.update(metric_list[key])
        
        self._vac_tenant_collector(metrics)
        self._vac_job_collector(metrics)
        self._vac_resource_collector(metrics)

        return defer.succeed(list(metrics.values()))


    def _vac_tenant_collector(self, metrics):
        """
        Get Tenant information
        :param metrics: Metrics list
        :return: List of Metrics
        """
        start = datetime.datetime.utcnow()
        log("Starting Tenat metrics collection")

        for tenant in self.tenants:
            labels = [str(tenant['id']), str(tenant['name'])]

            metrics['vac_tenant_vms_backedup'].add_metric(labels, tenant['vMsBackedUp'])
            metrics['vac_tenant_vms_replicated'].add_metric(labels, tenant['vMsReplicated'])
            metrics['vac_tenant_vms_backed_cloud'].add_metric(labels, tenant['vMsBackedUpToCloud'])

        log("Finished Tenant metrics collection (%s)", datetime.datetime.utcnow() - start)

        return list(metrics.values())


    def _vac_job_collector(self, metrics):
        """
        Collect Job Data
        :param metrics: Metrics list
        :return: List of Metrics
        """

        log("Gathering VAC Jobs")
        start = datetime.datetime.utcnow()
        headers = {'Authorization': self.token}
        url = "https://" + self.host + ":1281/v2/jobs"
        try:
            if self.ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Jobs (%s)",datetime.datetime.utcnow() - start)
                jobs = json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Jobs (%s)",datetime.datetime.utcnow() - start)
                jobs = json.loads(r.content)
        except Exception as error:
            log("Unable to gather jobs: (%s)", error)

        log("Starting Job metrics collection")

        labels = {}
        try:
            for job in jobs:
                tenant_id = job['_links']['tenants']['href'].split("/")

                processing_rate = str(job['processingRate']) + " " + job['processingRateUnits']
                transferred_rate = str(job['transferredData']) + " " + job['transferredDataUnits']
                defs = {'GB/s':1024**3, 'MB/s':1024**2, 'KB/s':1024, 'B/s':1, 'GB':1024**3, 'MB':1024**2, 'KB':1024, 'B':1}

                processing_rate_bytes = processing_rate.split()
                processing_rate_bytes = float(processing_rate_bytes[0]) * defs[processing_rate_bytes[1]]
                transferred_rate_bytes = transferred_rate.split()
                transferred_rate_bytes = float(transferred_rate_bytes[0]) * defs[transferred_rate_bytes[1]]

                labels = [str(job['id']),str(job['name']),str(tenant_id[(len(tenant_id)-1)])]

                metrics['vac_job_processing_rate_bytes'].add_metric(labels, float(processing_rate_bytes))
                metrics['vac_job_transfer_data_bytes'].add_metric(labels, float(transferred_rate_bytes))
                metrics['vac_job_protected_vms'].add_metric(labels, job['protectedVMs'])
        except Exception as error:
            log("Unable to parse job: " + str(error))

        log("Finished Job metrics collection (%s)", datetime.datetime.utcnow() - start)
        return list(metrics.values())


    def _vac_resource_collector(self, metrics):
        """
        Collect Backup Resources Data
        :param metrics: Metrics list
        :return: List of Metrics
        """
        log("Starting Resource collection")
        start = datetime.datetime.utcnow()

        labels = {}
        for tenant in self.tenants:
            try:
                resources = self._vac_tenant_resources(tenant['id'])
                
                labels = [str(resources[0]['id']),str(tenant['id']),str(tenant['name'])]

                used_storage_quota_rate = [str(resources[0]['usedStorageQuota']) + " " + resources[0]['usedStorageQuotaUnits']]
                used_traffic_quota_rate = [str(resources[0]['usedTrafficQuota']) + " " + resources[0]['usedTrafficQuotaUnits']]
                traffic_quota_rate = [str(resources[0]['trafficQuota']) + " " + resources[0]['trafficQuotaUnits']]
                storage_quota_rate = [str(resources[0]['storageQuota']) + " " + resources[0]['storageQuotaUnits']]

                defs = {'GB/s':1024**3, 'MB/s':1024**2, 'KB/s':1024, 'B/s':1, 'GB':1024**3, 'MB':1024**2, 'KB':1024, 'B':1}
                
                used_storage_quota_bytes = [float(lh)*defs[rh] for lh, rh in [e.split() for e in used_storage_quota_rate]]
                used_traffic_quota_bytes = [float(lh)*defs[rh] for lh, rh in [e.split() for e in used_traffic_quota_rate]]
                traffic_quota_bytes = [float(lh)*defs[rh] for lh, rh in [e.split() for e in traffic_quota_rate]]
                storage_quota_bytes = [float(lh)*defs[rh] for lh, rh in [e.split() for e in storage_quota_rate]]

                metrics['vac_resources_storage_quota_bytes'].add_metric(labels, float(storage_quota_bytes[0]))
                metrics['vac_resources_vms_quota'].add_metric(labels, resources[0]['vMsQuota'])
                metrics['vac_resources_traffic_quota_bytes'].add_metric(labels, float(traffic_quota_bytes[0]))
                metrics['vac_resources_used_storage_bytes'].add_metric(labels, float(used_storage_quota_bytes[0]))
                metrics['vac_resources_used_traffic_bytes'].add_metric(labels, float(used_traffic_quota_bytes[0]))
            except Exception as error:
                log("Unable to gather resources for (%s) (%s): (%s)", tenant['id'], tenant['name'], error)
                continue

        log("Finished resource metrics collection (%s)", datetime.datetime.utcnow() - start)
        return list(metrics.values()) 

    
    def _vac_tenant_resources(self, tenant_id):
        """
        Invoke Rest call for Tenant Resources
        :param tenant_id: Id of tenant
        :return: JSON object of tenant resources
        """
        log("Gathering resources for tenant {}".format(tenant_id))
        start = datetime.datetime.utcnow()
        headers = {'Authorization': self.token}
        url = "https://" + self.host + ":1281/v2/tenants/" + str(tenant_id) + "/backupResources"
        try:
            if self.ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Tenant resources (%s)", datetime.datetime.utcnow() - start)
                return json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Tenant resources (%s)", datetime.datetime.utcnow() - start)
                return json.loads(r.content)
        except Exception as error:
            log("Unable to gather tenant resoucres: (%s)", error)


class HealthzResource(Resource):
    """
    Class for exporter health
    """

    def render_GET(self, request):
        request.setHeader("Content-Type", "text/plain; charset=UTF-8")
        request.setResponseCode(200)
        log("Service is UP")
        return 'Server is UP'.encode()


class MetricsResource(Resource):
    """
    Class for exporter metrics
    """

    def render_GET(self, request):
        request.setHeader("Content-Type", "text/plain; charset=UTF-8")
        request.setResponseCode(200)
        log("Metrics")
        return generate_latest()


def _set_connection(username, password, host, ignore_ssl):
        """
        Connect to VAC and get connection
        :param username: Username of Vac user
        :param password: Password for Vac user
        :param host: Vac host to connect to
        :param ignore_ssl: Boolean for SSL cert verification
        :return: Dict of connection information
        """
        log("Initalizing connection")
        body = "grant_type=password&username=" + username + "&password=" + password
        headers = {'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': 'Bearer'}
        url = "https://" + host + ":1281/token"
        auth = {}
        try:
            if ignore_ssl:
                r = requests.post(url, data=body, headers=headers, verify=False)
                response = json.loads(r.content)
                auth['token'] = "Bearer " + response['access_token']
            else:
                r = requests.post(url, data=body, headers=headers)
                response = json.loads(r.content)
                auth['token'] = "Bearer " + response['access_token']
        except Exception as error:
            log("Unable to gather token: (%s)", error)

        log("Gathering VAC Tenants")
        start = datetime.datetime.utcnow()
        headers = {'Authorization': auth['token']}
        url = "https://" + host + ":1281/v2/tenants"
        try:
            if ignore_ssl:
                r = requests.get(url, headers=headers, verify=False)
                log("Fetched Tenants (%s)", datetime.datetime.utcnow() - start)
                auth['tenants'] = json.loads(r.content)
            else:
                r = requests.get(url, headers=headers)
                log("Fetched Tenants (%s)", datetime.datetime.utcnow() - start)
                auth['tenants'] = json.loads(r.content)

        except Exception as error:
            log("Unable to gather tenants: (%s)", error)

        log("Established connection")
        return defer.succeed(auth)


def main(argv=None):
    """
    Main entry point.
    """

    parser = ArgumentParser(description='VAC metrics exporter for Prometheus')
    parser.add_argument('-c', '--config', dest='config_file',
                        default=None, help="configuration file")
    parser.add_argument('-p', '--port', dest='port', type=int,
                        default=9273, help="HTTP port to expose metrics")

    args = parser.parse_args(argv or sys.argv[1:])
    
    reactor.suggestThreadPoolSize(25)

    root = Resource()
    root.putChild(b'healthz',HealthzResource())
    root.putChild(b'metrics',MetricsResource())
    root.putChild(b'vac',VacApplicationResource(args))

    factory = Site(root)
    endpoint = endpoints.TCP4ServerEndpoint(reactor, args.port)
    endpoint.listen(factory)
    reactor.run()


if __name__ == '__main__':
    main()
